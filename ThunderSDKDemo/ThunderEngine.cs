﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThunderSDKDemo
{

    using System;
    using System.Runtime.InteropServices;


    /// <summary>
    /// 迅雷下载引擎
    /// </summary>
    public static class ThunderEngine
    {
        public partial class NativeMethods
        {
            private const string ThunderSDK = @".\ThunderSDK\DownloadSDK.dll";

            [DllImport(ThunderSDK, CharSet = CharSet.Unicode)]
            public static extern bool XL_Init();

            [DllImport(ThunderSDK, CharSet = CharSet.Unicode)]
            public static extern bool XL_UnInit();

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr XL_CreateTask([In()]DownTaskParam stParam);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_StartTask(IntPtr hTask);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_StopTask(IntPtr hTask);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern object XL_SetSpeedLimit(Int32 nKBps);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern int XL_CreateTaskByThunder(string pszUrl, string pszFileName, string pszReferUrl, string pszCharSet, string pszCookie);

            [System.Runtime.InteropServices.DllImportAttribute(ThunderSDK, EntryPoint = "XL_CreateBTTaskByThunder")]
            public static extern int XL_CreateBTTaskByThunder([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string pszPath);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern object XL_SetUploadSpeedLimit(Int32 nTcpKBps, Int32 nOtherKBps);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern object XL_SetProxy(DOWN_PROXY_INFO stProxyInfo);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern object XL_DelTempFile(DownTaskParam stParam);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_DeleteTask(IntPtr hTask);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_GetBtDataFileList(string szFilePath, string szSeedFileFullPath);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_SetUserAgent(string pszUserAgent);

            [DllImport(ThunderSDK, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool XL_GetFileSizeWithUrl(string lpURL, Int64 iFileSize);

            [DllImportAttribute(ThunderSDK, EntryPoint = "XL_QueryTaskInfoEx", CallingConvention = CallingConvention.Cdecl)]
            [return: MarshalAsAttribute(UnmanagedType.Bool)]
            public static extern bool XL_QueryTaskInfoEx(IntPtr hTask, [Out()]DownTaskInfo stTaskInfo);

            [DllImportAttribute(ThunderSDK, EntryPoint = "XL_QueryTaskInfoEx", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr XL_CreateBTTask(DownBTTaskParam stParam);

            [DllImportAttribute(ThunderSDK, EntryPoint = "XL_QueryTaskInfoEx", CallingConvention = CallingConvention.Cdecl)]
            public static extern long XL_QueryBTFileInfo(IntPtr hTask, UIntPtr dwFileIndex, ulong ullFileSize, ulong ullCompleteSize, UIntPtr dwStatus);

            [DllImportAttribute(ThunderSDK, EntryPoint = "XL_QueryTaskInfoEx", CallingConvention = CallingConvention.Cdecl)]
            public static extern long XL_QueryBTFileInfo(IntPtr hTask, BTTaskInfo pTaskInfo);

        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
        public class DownTaskParam
        {
            public int nReserved;
            /// <summary>
            /// 任务URL
            /// </summary>
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 2084)]
            public string szTaskUrl;
            /// <summary>
            /// 引用页
            /// </summary>
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 2084)]
            public string szRefUrl;
            /// <summary>
            /// 浏览器cookie
            /// </summary>
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 4096)]
            public string szCookies;
            /// <summary>
            /// 下载保存文件名
            /// </summary>
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szFilename;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szReserved0;
            /// <summary>
            /// 文件保存目录
            /// </summary>
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szSavePath;
            public IntPtr hReserved;
            public int bReserved = 0;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szReserved1;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szReserved2;
            /// <summary>
            /// 是否只从原始地址下载
            /// </summary>
            public int IsOnlyOriginal = 0;
            public uint nReserved1 = 5;
            /// <summary>
            /// 禁止智能命名
            /// </summary>
            public int DisableAutoRename = 0;
            /// <summary>
            /// 是否用续传
            /// </summary>
            public int IsResume = 1;
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 2048, ArraySubType = UnmanagedType.U4)]
            public uint[] reserved;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct BTTaskInfo
        {

            ///LONG->int

            public int lTaskStatus;
            ///DWORD->unsigned int

            public uint dwUsingResCount;
            ///DWORD->unsigned int

            public uint dwSumResCount;
            ///ULONGLONG->unsigned __int64

            public ulong ullRecvBytes;
            ///ULONGLONG->unsigned __int64

            public ulong ullSendBytes;
            ///BOOL->int
            [MarshalAsAttribute(UnmanagedType.Bool)]

            public bool bFileCreated;
            ///DWORD->unsigned int

            public uint dwSeedCount;
            ///DWORD->unsigned int

            public uint dwConnectedBTPeerCount;
            ///DWORD->unsigned int

            public uint dwAllBTPeerCount;
            ///DWORD->unsigned int
            public uint dwHealthyGrade;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public class DownTaskInfo
        {
            public DOWN_TASK_STATUS stat;
            public TASK_ERROR_TYPE fail_code;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szFilename;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szReserved0;
            /// <summary>
            /// 该任务总大小(字节)
            /// </summary>
            public long nTotalSize;
            /// <summary>
            /// 下载有效字节数(可能存在回退的情况)
            /// </summary>
            public long nTotalDownload;
            /// <summary>
            /// 下载进度
            /// </summary>
            public float fPercent;
            public int nReserved0;
            /// <summary>
            /// 总资源数
            /// </summary>
            public int nSrcTotal;
            /// <summary>
            /// 可用资源数
            /// </summary>
            public int nSrcUsing;
            public int nReserved1;
            public int nReserved2;

            public int nReserved3;
            ///int

            public int nReserved4;
            ///__int64

            public long nReserved5;
            /// <summary>
            /// p2p贡献字节数
            /// </summary>
            public long nDonationP2P;
            ///__int64

            public long nReserved6;
            /// <summary>
            /// 原始资源共享字节数
            /// </summary>
            public long nDonationOrgin;
            /// <summary>
            /// 镜像资源共享字节数
            /// </summary>
            public long nDonationP2S;
            ///__int64

            public long nReserved7;
            ///__int64

            public long nReserved8;
            /// <summary>
            /// 即时速度(字节/秒)
            /// </summary>
            public int nSpeed;
            /// <summary>
            /// 即时速度(字节/秒)
            /// </summary>
            public int nSpeedP2S;
            /// <summary>
            /// 即时速度(字节/秒)
            /// </summary>
            public int nSpeedP2P;
            /// <summary>
            /// 原始资源是否有效
            /// </summary>
            public bool bIsOriginUsable;
            /// <summary>
            /// 现不提供该值
            /// </summary>
            public float fHashPercent;
            /// <summary>
            /// 是否正在创建文件
            /// </summary>
            public int IsCreatingFile;
            ///DWORD[64]
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.U4)]
            public uint[] reserved;
        }

        public enum DOWN_TASK_STATUS
        {

            ///NOITEM -> 0
            NOITEM = 0,

            TSC_ERROR,

            TSC_PAUSE,

            TSC_DOWNLOAD,

            TSC_COMPLETE,

            TSC_STARTPENDING,

            TSC_STOPPENDING
        }

        public enum TASK_ERROR_TYPE
        {

            /// <summary>
            /// 未知错误
            /// </summary>
            TASK_ERROR_UNKNOWN = 0,

            /// <summary>
            /// 创建文件失败
            /// </summary>
            TASK_ERROR_DISK_CREATE = 1,

            /// <summary>
            /// 写文件失败
            /// </summary>
            TASK_ERROR_DISK_WRITE = 2,

            /// <summary>
            /// 读文件失败
            /// </summary>
            TASK_ERROR_DISK_READ = 3,

            /// <summary>
            /// 重命名失败
            /// </summary>
            TASK_ERROR_DISK_RENAME = 4,

            /// <summary>
            /// 文件片校验失败
            /// </summary>
            TASK_ERROR_DISK_PIECEHASH = 5,

            /// <summary>
            /// 文件全文校验失败
            /// </summary>
            TASK_ERROR_DISK_FILEHASH = 6,

            /// <summary>
            /// 删除文件失败失败
            /// </summary>
            TASK_ERROR_DISK_DELETE = 7,

            /// <summary>
            /// 无效的DOWN地址
            /// </summary>
            TASK_ERROR_DOWN_INVALID = 16,

            /// <summary>
            /// 代理类型未知
            /// </summary>
            TASK_ERROR_PROXY_AUTH_TYPE_UNKOWN = 32,

            /// <summary>
            /// 代理认证失败
            /// </summary>
            TASK_ERROR_PROXY_AUTH_TYPE_FAILED = 33,

            /// <summary>
            /// http下载中无ip可用
            /// </summary>
            TASK_ERROR_HTTPMGR_NOT_IP = 48,

            /// <summary>
            /// 任务超时
            /// </summary>
            TASK_ERROR_TIMEOUT = 64,

            /// <summary>
            /// 任务取消
            /// </summary>
            TASK_ERROR_CANCEL = 65,

            /// <summary>
            /// MINITP崩溃
            /// </summary>
            TASK_ERROR_TP_CRASHED = 66,

            /// <summary>
            /// TaskId 非法
            /// </summary>
            TASK_ERROR_ID_INVALID = 67
        }

        public enum DOWN_PROXY_TYPE
        {

            ///PROXY_TYPE_IE -> 0
            PROXY_TYPE_IE = 0,

            ///PROXY_TYPE_HTTP -> 1
            PROXY_TYPE_HTTP = 1,

            ///PROXY_TYPE_SOCK4 -> 2
            PROXY_TYPE_SOCK4 = 2,

            ///PROXY_TYPE_SOCK5 -> 3
            PROXY_TYPE_SOCK5 = 3,

            ///PROXY_TYPE_FTP -> 4
            PROXY_TYPE_FTP = 4,

            ///PROXY_TYPE_UNKOWN -> 255
            PROXY_TYPE_UNKOWN = 255
        }


        public enum DOWN_PROXY_AUTH_TYPE
        {

            ///PROXY_AUTH_NONE -> 0
            PROXY_AUTH_NONE = 0,

            PROXY_AUTH_AUTO,

            PROXY_AUTH_BASE64,

            PROXY_AUTH_NTLM,

            PROXY_AUTH_DEGEST,

            PROXY_AUTH_UNKOWN
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public class DOWN_PROXY_INFO
        {

            ///BOOL->int
            [MarshalAsAttribute(UnmanagedType.Bool)]

            public bool bIEProxy;
            ///BOOL->int
            [MarshalAsAttribute(UnmanagedType.Bool)]

            public bool bProxy;
            ///DOWN_PROXY_TYPE

            public DOWN_PROXY_TYPE stPType;
            ///DOWN_PROXY_AUTH_TYPE

            public DOWN_PROXY_AUTH_TYPE stAType;
            ///wchar_t[2048]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 2048)]

            public string szHost;
            ///INT32->int

            public int nPort;
            ///wchar_t[50]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 50)]

            public string szUser;
            ///wchar_t[50]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 50)]

            public string szPwd;
            ///wchar_t[2048]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 2048)]
            public string szDomain;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class TrackerInfo
        {
            ///TCHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string szTrackerUrl;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class DownBTTaskParam
        {

            ///TCHAR[260]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]

            public string szSeedFullPath;
            ///TCHAR[260]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]

            public string szFilePath;
            ///DWORD->unsigned int

            public uint dwNeedDownloadFileCount;
            ///DWORD*

            public IntPtr dwNeedDownloadFileIndexArray;
            ///DWORD->unsigned int

            public uint dwTrackerInfoCount;
            ///TrackerInfo*

            public IntPtr pTrackerInfoArray;
            ///BOOL->int
            public int IsResume;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class tracker_info
        {

            ///DWORD->unsigned int

            public uint tracker_url_len;
            ///CHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string tracker_url;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class bt_file_info
        {

            ///ULONGLONG->unsigned __int64

            public ulong file_size;
            ///DWORD->unsigned int

            public uint path_len;
            ///CHAR[256]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 256)]

            public string file_path;
            ///DWORD->unsigned int

            public uint name_len;
            ///CHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string file_name;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class bt_seed_file_info
        {

            ///CHAR[20]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 20)]

            public string info_id;
            ///DWORD->unsigned int

            public uint title_len;
            ///CHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]

            public string title;
            ///DWORD->unsigned int

            public uint file_info_count;
            ///bt_file_info*

            public IntPtr file_info_array;
            ///DWORD->unsigned int

            public uint tracker_count;
            ///tracker_info*

            public IntPtr tracker_info_array;
            ///DWORD->unsigned int

            public uint publisher_len;
            ///CHAR[8192]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 8192)]

            public string publisher;
            ///DWORD->unsigned int

            public uint publisher_url_len;
            ///CHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string publisher_url;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class bt_data_file_item
        {

            ///DWORD->unsigned int

            public uint path_len;
            ///CHAR[256]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 256)]

            public string file_path;
            ///DWORD->unsigned int

            public uint name_len;
            ///CHAR[1024]
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string file_name;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public class bt_data_file_list
        {
            public uint item_count;
            public IntPtr item_array;
        }
    }
}
