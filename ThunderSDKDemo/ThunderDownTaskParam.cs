﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThunderSDKDemo
{
    /// <summary>
    /// 迅雷下载任务
    /// </summary>
    public struct ThunderDownTaskParam
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { set; get; }

        /// <summary>
        /// 是否立即下载
        /// </summary>
        public bool IsDownLoadNow { set; get; }

        /// <summary>
        /// 下载任务url
        /// </summary>
        public string TaskUrl { set; get; }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { set; get; }

        /// <summary>
        /// 保存路径，目录名称
        /// </summary>
        public string SavePath { set; get; }

        /// <summary>
        /// 是否断点续传
        /// </summary>
        public bool IsResume { set; get; }

        /// <summary>
        /// 默认扩展名
        /// </summary>
        public string DefaultExtension { set; get; }
    }

    /// <summary>
    /// HTTP进度信息
    /// </summary>
    public class HttpProgressInfo
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { set; get; }

        /// <summary>
        /// 下载进度类型
        /// </summary>
        public DownLoadTaskStatus Status { set; get; }

        /// <summary>
        /// 总大小
        /// </summary>
        public long TotalValue { set; get; }

        /// <summary>
        /// 当前进度
        /// </summary>
        public long CurrentValue { set; get; }
    }

    /// <summary>
    /// 下载任务状态
    /// </summary>
    public enum DownLoadTaskStatus
    {
        /// <summary>
        /// 开始下载
        /// </summary>
        Start,
        /// <summary>
        /// 正在下载
        /// </summary>
        DownLoading,
        /// <summary>
        /// 下载完成
        /// </summary>
        Finish,
        /// <summary>
        /// 下载暂停
        /// </summary>
        Pause,
        /// <summary>
        /// 下载出错
        /// </summary>
        Error,
    }
}
