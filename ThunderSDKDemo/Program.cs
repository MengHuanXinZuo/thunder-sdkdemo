﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThunderSDKDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ThunderDownLoadCore thunderDownLoadCore = ThunderDownLoadCore.Instance;

            thunderDownLoadCore.CreateThunderTask(new ThunderDownTaskParam()
            {
                TaskName = DateTime.Now.ToLongTimeString(),
                IsDownLoadNow = true,
                FileName = "测试下载.exe",
                SavePath = $@"{System.IO.Directory.GetCurrentDirectory()}\",
                TaskUrl = @"https://down.360safe.com/inst.exe"
            }, out string error);

            Console.WriteLine(error);

            Console.ReadKey();
        }
    }
}
